#include <ili9488_touch.h>
#include <ili9488.h>
#include <Servo.h>
#include <Wire.h>
#include <VL53L0X.h>
const int LASER_PIN = 2;
int input = -123;
int newInput = 0;
#define motorSpeed 100
#define m0 13
#define m1 A8
#define m2 10
#define m3 A11
#define mb0 12
#define mb1 A9
#define mb2 11
#define mb3 A10
int right = 0, left = 0, rightB = 0, leftB = 0;
void motors(int left, int right,int back_l, int back_r);

bool switch_state = false;

void setup() 
{
  pinMode(m0, OUTPUT);
  pinMode(m1, OUTPUT);
  pinMode(m2, OUTPUT);
  pinMode(m3, OUTPUT);
  pinMode(mb0, OUTPUT);
  pinMode(mb1, OUTPUT);
  pinMode(mb2, OUTPUT);
  pinMode(mb3, OUTPUT);
  pinMode (LASER_PIN, OUTPUT);
  motors(0,0,0,0);
  Serial1.begin(9600);
  Serial.begin(9600);
    while (!Serial1) {
    ; // wait for serial port to connect. Needed for Native USB only
  }
  
  lcd.Init_LCD();
  lcd.Set_Rotation(2);
  
  lcd.Set_Text_Back_colour(WHITE);
  lcd.Fill_Screen(YELLOW); 
  lcd.Set_Text_Size(2);
  
  lcdT.TP_Init();

  OffState();
}

void loop() 
{
  if (lcdT.TP_Scan()) 
  {
    int x = lcdT.getX();
    int y = lcdT.getY();

    if(switch_state) {
      if((x >= 109 && x <= 159)  && (y >= 214 && y <= 264)) {
        switch_state = false;
        OffState();
      }
    }
    else {
      if((x >= 160 && x <= 209)  && (y >= 214 && y <= 264)) {
        switch_state = true;
        OnState();
      }
    }
  }
}

void motors(int left, int right,int back_l, int back_r)
{
  if (left >= 0)
  {
    analogWrite(m0, left);
    digitalWrite(m1, 0);
  }
  else
  {
    analogWrite(m0, left);
    digitalWrite(m1, 1);
  }
  if (right >= 0)
  {
    analogWrite(m2, right);
    digitalWrite(m3, 0);
  }
  else
  {
    analogWrite(m2, right);
    digitalWrite(m3, 1);
  }
  if (back_l >= 0)
  {
    analogWrite(mb0, back_l);
    digitalWrite(mb1, 0);
  }
  else
  {
    analogWrite(mb0,back_l);
    digitalWrite(mb1, 1);
  }
  if (back_r >= 0)
  {
    analogWrite(mb2, back_r);
    digitalWrite(mb3, 0);
  }
  else
  {
    analogWrite(mb2, back_r);
    digitalWrite(mb3, 1);
  }

}

void OffState()
{
  lcd.Set_Draw_color(WHITE);
  lcd.Fill_Rectangle(109, 214, 159,264);
  lcd.Set_Text_colour(RED);
  lcd.Print("OFF", 119, 234);
                                  
  lcd.Set_Draw_color(BLACK);
  lcd.Fill_Rectangle(160, 214, 209, 264);
  motors(0,0,0,0);  
}

void OnState()
{
  lcd.Set_Draw_color(BLACK);
  lcd.Fill_Rectangle(109, 214, 159,264);
                                  
  lcd.Set_Draw_color(WHITE);
  lcd.Fill_Rectangle(160, 214, 209, 264);
  lcd.Set_Text_colour(GREEN);
  lcd.Print("ON", 174, 234);
  do
  {
    if (Serial1.available())
    {
      input = (int)(Serial1.read());
      Serial.println(input);
      motors(leftB, right, rightB, left);
      if (input != newInput)
      {
        switch(input)
        {
          case 48:
          {
              leftB = motorSpeed;
              right = motorSpeed;
              rightB = motorSpeed;
              left = motorSpeed;
              newInput = 48;
              break;
          }
          case 49:
          {
              leftB = -motorSpeed;
              right = -motorSpeed;
              rightB = -motorSpeed;
              left = -motorSpeed;
              newInput = 49;
              break;
          }
          case 50:
          {
              leftB = -motorSpeed;
              right = -motorSpeed;
              rightB = motorSpeed;
              left = motorSpeed;
              newInput = 50;
              break;
          }
          case 51:
          {
              leftB = motorSpeed;
              right = motorSpeed;
              rightB = -motorSpeed;
              left = -motorSpeed;
              newInput = 51;
              break;
          }
          case 52:
          {
              leftB = motorSpeed;
              right = motorSpeed;
              rightB = 0;
              left = 0;
              newInput = 52;
              break;
          }
          case 53:
          {
              leftB = 0;
              right = 0;
              rightB = motorSpeed;
              left = motorSpeed;
              newInput = 53;
              break;
          }
          case 54:
          {
              leftB = 0;
              right = 0;
              rightB = -motorSpeed;
              left = -motorSpeed;
              newInput = 54;
              break;
          }
          case 55:
          {
              leftB = -motorSpeed;
              right = -motorSpeed;
              rightB = 0;
              left = 0;
              newInput = 55;
              break;
          }
          case 56:
          {
              leftB = 0;
              right = 0;
              rightB = 0;
              left = 0;
              newInput = 56;
              break;
          }
          case 121:
          {
            digitalWrite(LASER_PIN, HIGH);
            newInput = 121;
            break;
          }
          case 110:
          {
            digitalWrite(LASER_PIN, LOW);
            newInput = 110;
            break;
          }
        }
      }
      motors(leftB, right, rightB, left);
  }
}while(!lcdT.TP_Scan());
  switch_state = false;
  OffState();
}
