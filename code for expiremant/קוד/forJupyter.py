import mne

mne.viz.set_3d_backend("notebook")


raw_mne = mne.io.read_raw_edf('sub3_after_19.01.21_19.15.01.edf')
ss = raw_mne.pick(range(4, 18))
ss.plot(lowpass=40, highpass=1)
ss.plot_psd(1.0, 50.0)
