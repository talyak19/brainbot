import websocket
import json
import ssl
import socket
import time

# define request id
QUERY_HEADSET_ID = 1
CONNECT_HEADSET_ID = 2
REQUEST_ACCESS_ID = 3
AUTHORIZE_ID = 4
CREATE_SESSION_ID = 5
SUB_REQUEST_ID = 6
SETUP_PROFILE_ID = 7
QUERY_PROFILE_ID = 8
TRAINING_ID = 9
DISCONNECT_HEADSET_ID = 10

LISTEN_PORT = 5378
channelsCount = 14
WINDOW = 1024
START_STREAM = "no"


class Cortex:
    def __init__(self, user, debug_mode=False):
        url = "wss://localhost:6868"
        self.ws = websocket.create_connection(url, sslopt={"cert_reqs": ssl.CERT_NONE})
        self.user = user
        self.debug = debug_mode

    def being_server(self):
        global sock
        global START_STREAM
        global client_addr
        sock = socket.socket(type=socket.SOCK_DGRAM)
        sock.bind(('', LISTEN_PORT))
        client_msg, client_addr = sock.recvfrom(1024)
        START_STREAM = client_msg.decode()
        print(START_STREAM)

    def query_headset(self):
        print('query headset --------------------------------')
        query_headset_request = {
            "jsonrpc": "2.0",
            "id": QUERY_HEADSET_ID,
            "method": "queryHeadsets",
            "params": {}
        }

        self.ws.send(json.dumps(query_headset_request, indent=4))
        result = self.ws.recv()
        result_dic = json.loads(result)
        print(result_dic)
        self.headset_id = result_dic['result'][0]['id']
        if self.debug:
            print('query headset result', json.dumps(result_dic, indent=4))

    def connect_headset(self):
        print('connect headset --------------------------------')
        connect_headset_request = {
            "jsonrpc": "2.0",
            "id": CONNECT_HEADSET_ID,
            "method": "controlDevice",
            "params": {
                "command": "connect",
                "headset": self.headset_id
            }
        }

        self.ws.send(json.dumps(connect_headset_request, indent=4))
        result = self.ws.recv()
        result_dic = json.loads(result)
        print(result_dic)
        if self.debug:
            print('connect headset result', json.dumps(result_dic, indent=4))

    def request_access(self):
        print('request access --------------------------------')
        request_access_request = {
            "jsonrpc": "2.0",
            "method": "requestAccess",
            "params": {
                "clientId": self.user['client_id'],
                "clientSecret": self.user['client_secret']
            },
            "id": REQUEST_ACCESS_ID
        }

        self.ws.send(json.dumps(request_access_request, indent=4))
        result = self.ws.recv()
        result_dic = json.loads(result)
        print(result_dic)
        if self.debug:
            print(json.dumps(result_dic, indent=4))

    def authorize(self):
        print('authorize --------------------------------')
        authorize_request = {
            "jsonrpc": "2.0",
            "method": "authorize",
            "params": {
                "clientId": self.user['client_id'],
                "clientSecret": self.user['client_secret'],
                "license": self.user['license'],
                "debit": self.user['debit']
            },
            "id": AUTHORIZE_ID
        }

        if self.debug:
            print('auth request \n', json.dumps(authorize_request, indent=4))

        self.ws.send(json.dumps(authorize_request))

        while True:
            result = self.ws.recv()
            result_dic = json.loads(result)
            print(result_dic)
            if 'id' in result_dic:
                if result_dic['id'] == AUTHORIZE_ID:
                    if self.debug:
                        print('auth result \n', json.dumps(result_dic, indent=4))
                    self.auth = result_dic['result']['cortexToken']
                    break

    def get_license_info(self):
        print('requesting -----------------')
        get_info_request = {
            "id": 1,
            "jsonrpc": "2.0",
            "method": "getLicenseInfo",
            "params": {
                "cortexToken": self.auth
            }
        }
        self.ws.send(json.dumps(get_info_request))
        result = self.ws.recv()
        result_dic = json.loads(result)
        print(result_dic)

    def create_session(self):
        print('create session --------------------------------')
        create_session_request = {
            "jsonrpc": "2.0",
            "id": CREATE_SESSION_ID,
            "method": "createSession",
            "params": {
                "cortexToken": self.auth,
                "headset": self.headset_id,
                "status": "active"
            }
        }

        if self.debug:
            print('create session request \n', json.dumps(create_session_request, indent=4))

        self.ws.send(json.dumps(create_session_request))
        result = self.ws.recv()
        result_dic = json.loads(result)
        print(result_dic)
        if self.debug:
            print('create session result \n', json.dumps(result_dic, indent=4))

        self.session_id = result_dic['result']['id']

    def close_session(self):
        print('close session --------------------------------')
        close_session_request = {
            "jsonrpc": "2.0",
            "id": CREATE_SESSION_ID,
            "method": "updateSession",
            "params": {
                "cortexToken": self.auth,
                "session": self.session_id,
                "status": "close"
            }
        }

        self.ws.send(json.dumps(close_session_request))
        result = self.ws.recv()
        result_dic = json.loads(result)
        print(result_dic)
        if self.debug:
            print('close session result \n', json.dumps(result_dic, indent=4))

    def sub_request(self, stream):
        global sock
        global client_addr
        print('subscribe request --------------------------------')

        counter = 0
        raw_data_file = open("output\\raw_data.txt", "w")

        sub_request_json = {
            "jsonrpc": "2.0",
            "method": "subscribe",
            "params": {
                "cortexToken": self.auth,
                "session": self.session_id,
                "streams": stream
            },
            "id": SUB_REQUEST_ID
        }
        print(sub_request_json)
        self.ws.send(json.dumps(sub_request_json))

        if 'sys' in stream:
            new_data = self.ws.recv()
            print(json.dumps(new_data, indent=4))
            print('\n')
        else:
            while True:
                counter += 1
                new_data = self.ws.recv()
                if START_STREAM == "send data":
                    sock.sendto(new_data.encode(), client_addr)
                    if counter == 42:
                        local_time = time.ctime(time.time())
                        print(local_time)
                        new_data = new_data.split(',')[2:-5]
                        raw_data_file.write(local_time + "\t" + str(new_data) + "\n")
                        counter = 0
                if "mot" in new_data:
                    print(new_data)
        raw_data_file.close()


def main():
    user = {"client_id": "Q231YGUU2BtNihVwInOwx6WFCAmq9fPz2KLFWkXB",
            "client_secret": "kolp0cQHEiyi19v34jNSNFKsMALnGBqttLBTuNU8OttBmUnP2hwdwNXork3Siex1p0czBXRgY9pv2oReupJEo7MrzAARoijOGpk07Gsk5j9aRvwY5cwX0rCcorMh3NfZ",
            "license": "f04707dc-b759-40ba-b260-5fb25a25a7ee",
            "debit": 10000}
    me = Cortex(user)
    Cortex.being_server(me)
    Cortex.request_access(me)
    Cortex.query_headset(me)
    Cortex.connect_headset(me)
    Cortex.authorize(me)
    Cortex.create_session(me)
    Cortex.sub_request(me, ["mot", "eeg"])


if __name__ == '__main__':
    main()
# -------------------------------------------------------------------
# -------------------------------------------------------------------
# -------------------------------------------------------------------
