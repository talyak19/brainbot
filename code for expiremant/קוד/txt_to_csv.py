import pandas as pd
import numpy as np

data = pd.read_csv('raw_data.txt', sep=",", header=None)
data.columns = ["time", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m"]

for i in range(0, 3612):   #3784 is for raw_data_test.txt. for raw_data_before.txt put 1075. for raw_data_after.txt put 731
    curr_time = data["time"][i].split("\t")[0]
    first = data["time"][i].split("\t")[1][2:-1]
    second = data["a"][i][2:-1]
    third = data["b"][i][2:-1]
    fourth = data["c"][i][2:-1]
    fifth = data["d"][i][2:-1]
    sixth = data["e"][i][2:-1]
    seventh = data["f"][i][2:-1]
    eighth = data["g"][i][2:-1]
    ninth = data["h"][i][2:-1]
    tenth = data["i"][i][2:-1]
    eleventh = data["j"][i][2:-1]
    twelfth = data["k"][i][2:-1]
    thirteenth = data["l"][i][2:-1]
    fourteenth = data["m"][i][2:-2]

    if i != 0:
        toWrite = np.append(toWrite, [[first, second, third, fourth, fifth, sixth, seventh, eighth, ninth, tenth, eleventh, twelfth, thirteenth, fourteenth]], axis=0)
    else:
        toWrite = np.array([[first, second, third, fourth, fifth, sixth, seventh, eighth, ninth, tenth, eleventh, twelfth, thirteenth, fourteenth]])
# toWrite has the data in it

print(toWrite)

pd.DataFrame(toWrite).to_csv("data.csv")
