import serial
import pygame
from pygame.locals import *

import keyboard

FILE_PATH = "ledOnOff.txt"
ser = serial.Serial('COM14', 9600)

button_to_move = {(False, False, True, False): b'0',
                  (False, False, False, True): b'1',
                  (False, True, False, False): b'2',
                  (True, False, False, False): b'3',
                  (True, False, True, False): b'4',
                  (False, True, True, False): b'5',
                  (True, False, False, True): b'6',
                  (False, True, False, True): b'7',
                  (False, False, False, False): b'8'
                  }


def read_from_file():
    with open(FILE_PATH, "r") as f:
        if f.read() == "y":
            ser.write(b'y')
        else:
            ser.write(b'n')


def main():
    pygame.init()
    pygame.joystick.init()
    joysticks = [pygame.joystick.Joystick(i) for i in range(pygame.joystick.get_count())]
    active_buttons = (0, 0, 0, 0)

    for joystick in joysticks:

        while True:
            for event in pygame.event.get():
                if event.type == JOYAXISMOTION:
                    x_axis = joystick.get_axis(0)
                    y_axis = joystick.get_axis(1)
                    active_buttons = (x_axis < -0.5, x_axis > 0.5, y_axis < -0.5, y_axis > 0.5)
            print("left, right, top, bottom", active_buttons)
            ser.write(button_to_move.get(active_buttons))
            read_from_file()


if __name__ == '__main__':
    main()
