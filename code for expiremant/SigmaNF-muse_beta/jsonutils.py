import json

## Elad class deffinitions
class StringFloat:
    def __init__(self, name, value):
        self.name = name
        self.value = value

    def tojson(self):
        return json.dumps(self, default=lambda o: o.__dict__,sort_keys=True)


class StringFloatContainer:
    def __init__(self,list):
        self.list = list

    def tojson(self):
        return json.dumps(self, default=lambda o: o.__dict__, sort_keys=True)
