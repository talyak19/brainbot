# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
from __future__ import division, print_function, absolute_import
import sys
from scipy.io import loadmat
import numpy as np
import pyedflib as edf



#%%
def readedf(filename="default.edf", verbos=True):

    # read edf file by means of pyedf package
    #   f: edfreader object
    try:
        f = edf.edfreader.EdfReader(filename)
    except IOError as e:
        print ("ERROR: fail to open file: {}\n\tPlease make shue that the file exists\n\t{}".format(filename, e.message))
        sys.exit(1)

    samples = f.getNSamples()[0]
    frequency = f.getSampleFrequency(0)
    n = f.signals_in_file
    signal_labels = f.getSignalLabels()

    if verbos:
        print("\nchannels:\t\t\t %i" % n)
        print("file duration:\t\t %i seconds" % f.file_duration)
        print("samples in file:\t %i" % samples)
        print("samplefrequency:\t {:10.1f}Hz\n" .format(frequency))

    sigbufs = np.zeros((n, f.getNSamples()[0]))
    for i in range(0, n):
        sigbufs[i, :] = f.readSignal(i)

    f._close()
    del f

    return (sigbufs, dict(channels=n, names=signal_labels, samples = samples, frequency = int(frequency)   ))


def read_eeg(filename, type='hd5', matvar='EEG', **kwargs):
    if 'sampling_rate' in kwargs:
        sampling_rate = kwargs['sampling_rate']
    else:
        sampling_rate = 256

    if type == 'edf':
        try:
            return readedf(filename)
        except IOError as err:
            print (err.message)
            return None, None

    if type == 'hd5':
        import h5py
        try:
            matf = h5py.File(filename, 'r')
            mat = matf[matvar].value.T
            return mat, {'channels': mat.shape[0], 'frequency': sampling_rate, 'samples':mat.shape[1]}
        except IOError as err:
            print (err.message)
            return None, None


    if type == 'mat':
        from scipy.io import loadmat
        try:
            mat = loadmat(filename, mdict=None, appendmat=True)[matvar]
            return mat, {'channels': mat.shape[0], 'frequency': sampling_rate, 'samples':mat.shape[1]}
        except IOError as err:
            print (err.message)
            return None, None
        except KeyError as err:
            print ("file is not in the correct format, try change the inner variable name to EEG")
            return None, None

    if type == 'eeglab':
        from scipy.io import loadmat
        eeglab = loadmat(filename, mdict=None, appendmat=True)['EEG'][0][0]
        mat = eeglab['data']
        return mat, {'channels': mat.shape[0], 'frequency': sampling_rate, 'samples':mat.shape[1]}

#%%



