import numpy as np
import matplotlib.pyplot as plt


def plotRaster(raster, t=None, axes = None):
    if t is not None:
        x = t
    else:
        x = np.arange(0, raster.shape[1])

    raster[raster > 0] = 1  # make all events equal 1


    for i in range(raster.shape[0]):
        y = raster[i, :] * i+1
        if axes is None:
            plt.scatter(x, y, s=10, c='g', alpha=0.1)
        else:
            axes.scatter(x, y, s=10, c='g', alpha=0.1)
    print ('raster drawn')


def plotSigma(s, time, avalanches, holdon=False, axes = None):
    """
    Plotting the contineous sigma parameter of the avalanche analysis
    s: sigma vector
    time: time vector
    avalanches: struct with avalanches analysis results
    [holdon]
    [axes]
    :rtype: object
    """
    sig_time = np.zeros(time.shape)

    if avalanches.size <= 1:
        return

    for i, a in zip(range(avalanches.size - 1), avalanches[:-1]):
        sig_time[a:avalanches[i+1]] = s[i]

    sig_time[avalanches[i+1]:] = s[i+1]

    # regularization
    sig_time[sig_time > (sig_time.std()*3)] = sig_time.std()*3

    return (time, sig_time)

def plotAvalanches(t_grid, tp_bin, scale=False, holdon=False, axes = None):

    if not holdon:
        plt.figure()

    if not scale:
        scale = t_grid.max()

    if axes is None:
        plt.plot(t_grid[:-1], tp_bin * scale, alpha=0.1)
    else:
        axes.plot(t_grid[:-1], tp_bin * scale, alpha=0.1)

