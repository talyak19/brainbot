function varargout = stream_gui2(varargin)
% STREAM_GUI2 MATLAB code for stream_gui2.fig
%      STREAM_GUI2, by itself, creates a new STREAM_GUI2 or raises the existing
%      singleton*.
%
%      H = STREAM_GUI2 returns the handle to a new STREAM_GUI2 or the handle to
%      the existing singleton*.
%
%      STREAM_GUI2('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in STREAM_GUI2.M with the given input arguments.
%
%      STREAM_GUI2('Property','Value',...) creates a new STREAM_GUI2 or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before stream_gui2_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to stream_gui2_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help stream_gui2

% Last Modified by GUIDE v2.5 03-Jul-2017 14:32:58

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @stream_gui2_OpeningFcn, ...
                   'gui_OutputFcn',  @stream_gui2_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before stream_gui2 is made visible.
function stream_gui2_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to stream_gui2 (see VARARGIN)

% Choose default command line output for stream_gui2
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes stream_gui2 wait for user response (see UIRESUME)
% uiwait(handles.figure1);
global modelName
% global start_time;
global cfg
cfg.streamType = 'lSL';

modelName = 'stream_model';
% start_time=7;


% --- Outputs from this function are returned to the command line.
function varargout = stream_gui2_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


function LoadModel()

global modelName
global Scope
global OriginalStopTime
global OriginalMode
global OriginalStartFcn

if isempty(find_system('Type','block_diagram','Name',modelName)),
    load_system(modelName);
end

% Store model's parameters, in order to restore them in the end
OriginalStopTime = get_param(modelName,'Stoptime');
OriginalMode = get_param(modelName,'SimulationMode');
OriginalStartFcn = get_param(modelName,'StartFcn');

% Add a listener to the 'Scope' block
Scope = struct(...
    'blockName','',...
    'blockHandle',[],...
    'blockEvent','',...
    'blockFcn',[]);

Scope.blockName = sprintf('%s/Scope',modelName);
Scope.blockHandle = get_param(Scope.blockName,'Handle');
Scope.blockEvent = 'PostOutputs';
Scope.blockFcn = @ScopeCallback;

% --- START BUTTON.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Scope
global cfg % configuration structure
global eventHandle
global TimeInd
global modelName
global isdone
global  stoph starth;
% global target
global socket
global d0
global is_shown
global EEG
global outlet % use for LSL comunication
global firstInvocation 

EEG = [];
cfg.streamType = 'LSL';

set(handles.pushbutton2,'enable','on')
set(handles.pushbutton1,'enable','off')
firstInvocation = 1;

is_shown = 0;

isdone=0;
TimeInd=0;

LoadModel();% Load the SIMULINK model

set_param(modelName,'SimulationMode','normal');   
%% Comunication setup
    disp('Initializing communivation...');
    streamtype = get(handles.streamtype,'string');
    switch streamtype
        case 'TCP'
            % get IP address from gui
            IP = get(handles.edit1,'string');
            PORT = str2double (get(handles.editport,'string'));
            % configure tcp socket with IP and port:
            % this is for 2014a oter versions will use tcpclient
            % TODO: change port number to global const or external congfigurauion
            socket = tcpip( IP ,PORT, 'NetworkRole', 'client'); % we can change the address(127.0.0.1) acording to the udp address of the specific computer we are yousing

            % open socket as file handler
            fopen(socket);

            d0 = createMessage(0,1); %define aas zero
            example_message= 1;
            d = createMessage(example_message,1); %sending the power value (we sending the power value multioly with 99999 for not confussing unity
            fwrite(socket,d);




        case 'UDP'
        case 'LSL' % LSL initialize 
            % instantiate the library
            disp('Loading library...');
            lib = lsl_loadlib();
            

            % make a new stream outlet
            disp('Creating a new streaminfo...');
            info = lsl_streaminfo(lib,'gHIamp','EEG',12,256,'cf_double64','sdfwerr32432');

            disp('Opening an outlet...');
            outlet = lsl_outlet(info);            
        otherwise
            
    end
    
    % start the model
    set_param(modelName,'SimulationCommand','start');

    % Set a listener
    eventHandle = add_exec_event_listener(Scope.blockName,Scope.blockEvent, Scope.blockFcn);


 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Callback Function for executing the event listener on the EEG Scope
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function ScopeCallback(block, eventdata) %#ok
global firstInvocation
global outlet % use for LSL comunnication 

streamtype = 'LSL';
if firstInvocation 
    disp (['sending data now...' streamtype]);
    firstInvocation = 0;
end
switch streamtype
    case 'LSL'
        val = block.InputPort(1).Data;
        outlet.push_sample(val);
    otherwise
end

    

% Stop Button
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global modelName
global isdone
isdone=1;
set(handles.pushbutton2,'enable','off')
set(handles.pushbutton1,'enable','on')


% stop the model
set_param(modelName,'SimulationCommand','stop');



function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editport_Callback(hObject, eventdata, handles)
% hObject    handle to editport (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editport as text
%        str2double(get(hObject,'String')) returns contents of editport as a double


% --- Executes during object creation, after setting all properties.
function editport_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editport (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in streamtype.
function streamtype_Callback(hObject, eventdata, handles)
% hObject    handle to streamtype (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns streamtype contents as cell array
%        contents{get(hObject,'Value')} returns selected item from streamtype


% --- Executes during object creation, after setting all properties.
function streamtype_CreateFcn(hObject, eventdata, handles)
% hObject    handle to streamtype (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
