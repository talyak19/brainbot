# -*- coding: utf-8 -*-
"""
Created on Wed Aug 10 14:15:38 2016

@author: Nir Getter
"""
import sys
import os
import numpy as np
import time
#from tqdm import tqdm
from readEEG import read_eeg
from scipy import stats
from pylsl import StreamInfo, StreamOutlet

from progress_bar import print_progress

CHUNK_SIZE = 1
MAX_BUFFER = 320
CHANNEL_COUNT = 20
SAMPLERATE = 256

FILENAME = r'sampledata\almog-session-3-5min-s4.mat'
FILETYPE = 'hd5'
MATVAR = 'EEG'
SIMULATE = True
NREPEAT = 1

try:
    if sys.argv[1]=='_DEBUG':
        print ("\n\n**********\n\033[93mDebug Mode\033[0m\n**********")
        from helperPlots import *
        _DEBUG = True
    else:
        print("\n\n*********\nRun Mode\n**********")
        _DEBUG = False
except:
    _DEBUG = False


sys.stdout.flush()
def log(msg):
    if _DEBUG:
        print(msg)

if getattr(sys, 'frozen', False):
        # we are running in a bundle
        frozen = 'ever so'
        bundle_dir = sys._MEIPASS
else:
        # we are running in a normal Python environment
        bundle_dir = os.path.dirname(os.path.abspath(__file__))

llimit = 0
hlimit = 'end'

# prepere a buffer from the edf file with initial pre processing
try:
    sigbufs, sigparams = read_eeg(os.path.join(bundle_dir, FILENAME), type=FILETYPE)
except IOError as ie:
    print ('%s Not Exist' % os.path.join(bundle_dir, FILENAME))
    print (ie.message)
    exit(0)


CHANNEL_COUNT = sigparams['channels']
log("samples to analyze:\t{}".format(sigbufs.shape[1]))

# generating the time vector of the recording
# the time vector is a linear interpolation between the lower time limit and top time limit
tl = 0
th = np.shape(sigbufs)[1] / int(sigparams["frequency"])  # the total buffer time is frame_count/frame_rate
log("recording time (sec):\t{}".format(th))

# the time vector is a linear space, size equal to buffer time dimension
# every member is one time sample (th) spaced from the previous
t = np.linspace(tl, th, np.shape(sigbufs)[1])

frame_time = t[1] - t[0]
log("frame time is:\t\t{:10.4f} ms".format(frame_time * 1000))

# lsl initializations

# first create a new stream info (here we set the name to BioSemi,
# the content-type to EEG, 8 channels, 100 Hz, and float-valued data) The
# last value would be the serial number of the device or some other more or
# less locally unique identifier for the stream as far as available (you
# could also omit it but interrupted connections wouldn't auto-recover).
info = StreamInfo('SimulateEEG', 'EEG', CHANNEL_COUNT, 100, 'float32', 'myuid34234')

# next make an outlet
outlet = StreamOutlet(info) #, CHUNK_SIZE, MAX_BUFFER)
for repeat in range(1, NREPEAT + 1):
    print("Now sending data...")
    t = time.clock()
    print (("Time: {}").format(t))

    for i, sample in zip(range(sigbufs.shape[1]), sigbufs.T):  # make a new random 8-channel sample; this is converted into a
        # pylsl.vectorf (the data type that is expected by push_sample)
        mysample = sample
        # now send it and wait for a bit
        outlet.push_sample(mysample)

        print_progress(i, sigbufs.shape[1], 'frame')
        if SIMULATE:
            time.sleep(frame_time)
