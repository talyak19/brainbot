# -*- coding: utf-8 -*-

from __future__ import print_function
import time
from scipy.io import savemat
import numpy as np
import socket
from jsonutils import StringFloat, StringFloatContainer
from nf_options import *
from threading import Thread
from bandpower_nf import bandpower_nf as nffunc
from nf_sigma import sigma_nf
from nf_stats import nf_stats, get_scale

SERVER_ADDR = "127.0.0.1"
SERVER_PORT = 5378
FILE_PATH = "ledOnOff.txt"
curr_value_in_file = 0

raw_data = []
sock = None
GATE = "-1"

try:
    from pylsl import StreamInlet, resolve_stream, pylsl, resolve_byprop
except RuntimeError as r:
    print('Error:\tcant import pylsl module\n{}'.format(r.__cause__))
    sys.exit(1)


def write_to_file(value):
    global curr_value_in_file
    if value == curr_value_in_file:
        return
    f = open(FILE_PATH, "w")
    f.write(value)
    f.close()
    curr_value_in_file = value


class nf(Thread):
    def __init__(self):
        Thread.__init__(self)
        print("init class %s" % type(self))
        self.sujectNumbr = SUBJECT
        self.sessionNumber = SESSION

    def initLSL(self):
        # create a new inlet to read from the stream
        self.inlet = 0  # StreamInlet(streams[0], max_buflen=MAX_BUFLEN, max_chunklen=MAX_CHUNKLEN, recover=False)

        global sock
        channelsCount = 14
        print("looking for an EEG stream...")
        sock = socket.socket(type=socket.SOCK_DGRAM)
        msg = "send data"
        sock.sendto(msg.encode(), (SERVER_ADDR, SERVER_PORT))

        return channelsCount

    def readLSL(self, wnd):
        global sock
        global raw_data
        for bufinx in range(BUFF, WINDOW):
            try:
                server_msg, server_addr = sock.recvfrom(1024)
                server_msg_after_decode = server_msg.decode()
                if "jsonrpc" not in server_msg_after_decode:
                    server_msg_after_decode = server_msg_after_decode.split(',')
                    for sensor in server_msg_after_decode[2:-5]:
                        if sensor is not None:
                            wnd[bufinx] = float(sensor)
                        else:
                            print('End Of Transmission\n')
                            sock.close()
                            wnd = None
                        raw_data.append(sensor)
                    sample = raw_data
                    timestamp = server_msg_after_decode[-1].split('"time":')[1].split("}")[0]

            except pylsl.LostError:
                wnd = None

        return wnd

    def run(self):
        global raw_data

        # init the inbound lsl communication
        channelsCount = self.initLSL()

        # send signal of waiting to unity

        onOrOff = StringFloatContainer([StringFloat("scene", str(1))]).tojson()
        if onOrOff >= GATE:
            write_to_file('y')
        else:
            write_to_file('n')

        # initiate number of reads (mainly for debug and impression)
        itCount = 0

        # initiate avalanche analysis log for debug and post analysis
        avanalysisLog = []

        # init the wnd buffer with BUFFER rows of channel_count collumns
        wnd = np.zeros((WINDOW, channelsCount))  # x is the x axis of the chart (timescale)

        # initiate time vector in length of WINDOW starting from 0
        timeVector = np.linspace(0, WINDOW, WINDOW)

        # init stats
        stats = np.array([])
        sig_run_mean = 0
        sig_run_sd = 0
        ava_count = 0

        # this is the main read > calculate sigma > transmit loop
        wnd_struct = []

        wnd_mean = np.zeros(channelsCount)
        wnd_std = np.zeros(channelsCount)
        a = WNDOVLP / HISTORY

        start_time = time.perf_counter()
        now_time = time.perf_counter() - start_time
        while now_time <= RUNTIME:
            now_time = time.perf_counter() - start_time

            itCount += 1

            # read LSL stream calculate and save into global wnd
            wnd = np.roll(wnd, -1 * WNDOVLP, 0)  # move wnd and clear space for new lsl buffer
            wnd = self.readLSL(wnd)  # read lsl samples into moving wnd

            wnd_mean_old = wnd_mean
            wnd_std_old = wnd_std

            if wnd.any() is not None:
                wnd_mean = (wnd_mean_old * a) + (np.mean(wnd, 0) * (1 - a))
                wnd_std = (wnd_std_old * a) + (np.std(wnd, 0) * (1 - a))
            else:
                print("\n\nTransmission ended.....")
                break

            if PROTOCOL == 1:
                # Compute the number of epochs in "buffer_length"
                n_win_test = int(np.floor((WINDOW - int(WINDOW / 2)) /
                                          WNDOVLP + 1))

                # Initialize the band power buffer (for plotting)
                # bands will be ordered: [delta, theta, alpha, beta]
                band_buffer = np.zeros((n_win_test, 4))
                sig_mean, bp = nffunc(wnd[:, CHANNEL_IND], 256, band_buffer, (1, 2))
                if sig_mean is None:
                    wnd_struct.append({'raw': wnd, 'beta_theta': 0, 'bp': bp, 'time': timeVector})
                else:
                    wnd_struct.append({'raw': wnd, 'beta_theta': sig_mean, 'bp': bp, 'time': timeVector})
            if PROTOCOL_DIRECTION == 1:
                nfparam = (sig_mean - NF_MIN) * SCALE
            else:
                nfparam = 1 - ((sig_mean - NF_MIN) * SCALE)
            #
            # send sigma mean as parameter over TCP socket tobe consume by display application such as unity3d game
            try:
                # here I am creating a json object to be sent over TCP with two parameters (currently they both hold the same value)
                toSend = StringFloatContainer([
                    StringFloat("input1", str(nfparam)),
                    StringFloat("time", str(now_time / RUNTIME)),
                ]).tojson()
            ########                resp = tcpOut.send(toSend.encode())

            except socket.error as msg:
                print("\n\n\033[93mERROR: comunication to unity aborted\n{}".format(msg.message))
                break  # abort execution with error code 1

            stats = nf_stats(sig_mean, self.sujectNumbr, self.sessionNumber, itCount, nfparam, stats)

            if SET_DYN_THRESHOLD > 0:
                pass

            # print results to console for debug and presentation purposes
            symbol = ['|', '/', '-', '\\']
            ########            if resp != len(str(nfparam)):  # if data sent different from intended
            ########                sys.stdout.write('\r[{}]{}\t {}:{}\t{}\t{}  ERROR'.format(
            ########                    symbol[itCount % 4], itCount,
            ########                    timeVector[0], timeVector[-1], wnd.shape,
            ########                    sig_mean))

            sys.stdout.write('\r[{}]{} {:2.0f}s\t {:.2f}[{:.2f}, {:.2f}]#{} >> \033[93m{:.2f}\033[39m'.format(
                symbol[itCount % 4], itCount, now_time,
                sig_mean, sig_run_mean, sig_run_sd, ava_count,
                nfparam))

        savemat('output/wnd_struct_{}_S{}'.format(self.sujectNumbr, self.sessionNumber), {'lsl_windows': wnd_struct})
        # send signal of waiting to unity
        toSend = StringFloatContainer([StringFloat("scene", str(2))]).tojson()
        ########        resp = tcpOut.send(toSend.encode())

        print('Summary\n---------------')
        summary = (
            stats.mean(),
            stats.std(),
            stats.min(),
            stats.max(),
            get_scale(stats)
        )
        print(
            'sigma stats:\n\tmean:\t{:+.2f}\n\tSD:\t{:+.2f}\n\trange:\t{:+.2f}....{:+.2f}\n\tnf_scale\t{:+.4f}\n'.format(
                *summary))
        print('\n** GAME OVER **')


if __name__ == '__main__':
    time.sleep(3)
    usage()
    processCommandlineOptions()
    nf_runner = nf()
    nf_runner.start()
    nf_runner.join()
    print("------ Endof __main__")
