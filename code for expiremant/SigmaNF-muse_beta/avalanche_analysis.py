from __future__ import print_function
import os
from scipy import stats
from readEEG import read_eeg
from scipy.io import savemat

import sys
import pandas as pd
import getopt

# bokeh imports
from bokeh.plotting import figure, show, output_file
from bokeh.layouts import gridplot

# self imports
from findavalanche import FindAvalnche as fa
from findavalanche import AvalancheNone
from helperPlots import *

# adapting to pyinstaller bundel
if getattr(sys, 'frozen', False):
        # we are running in a bundle
        frozen = 'ever so'
        bundle_dir = sys._MEIPASS
else:
        # we are running in a normal Python environment
        bundle_dir = os.path.dirname(os.path.abspath(__file__))

#######
##
## Options decleration and updating
##
TAU_RANGE = range(6, 18)
TH_RANGE = np.arange(2, 3, 1)


SUBJECT = '000'
def opt_subject(a):
    global SUBJECT
    SUBJECT = np.float64(a)

INPUTFILE = os.path.join(bundle_dir, 'output.mat')
def opt_file(a):
    global INPUTFILE
    global bundle_dir
    INPUTFILE = a

    if os.path.dirname(a) != '':
        bundle_dir = os.path.dirname(a)

MATVAR = 'EEG'
def opt_var(a):
    global MATVAR
    MATVAR = a

FILETYPE = 'mat'
def opt_type(a):
    global FILETYPE
    FILETYPE = a

def opt_help():
    print ("""
    --subject=      subject number. default 0000
    --file=         file for the analysis
    --type=         [hd5 | edf | eeglab | mat]
    --var=          when type=mat.hd5 get the var with the data default=EEG
    --help          show this text
    """)
    exit(0)

ops_list = ['subject=', 'file=', 'type=', 'var=', 'help']
ops_dict = {
    '--subject': opt_subject,
    '--type'   : opt_type,
    '--file'   : opt_file,
    '--var'    : opt_var,
    '--help': opt_help,
}

def processCommandlineOptions():
    # get init options from command line
    try:
        opts, args = getopt.getopt(sys.argv[1:], 'x', ops_list)
    except getopt.GetoptError as err:
        # print help information and exit:
        print(str(err))  # will print something like "option -a not recognized"
        opt_help()
        sys.exit(2)

    # activate opts functions
    for opt, a in opts:
        if a == '':
            ops_dict[opt]()
        else:
            ops_dict[opt](a)

    print('input file is: %s' % INPUTFILE)
    print('file type is set to: %s' % FILETYPE)
    if FILETYPE=='mat.hd5':
        print('matlab variable is set to: %s' % MATVAR)
    print('subject is: %s' % SUBJECT)


########################################## Options end

def log(msg):
    print(msg)


def valOrNone(v, val=None):
    if val is None:
        return 'None' if v is None else v
    else:
        return val()


if __name__ == "__main__":
    processCommandlineOptions()

    llimit = 500
    hlimit = 'end'

    # create sigbuff which is a buffer of all data in the eeg file to be analyzed.
    # the shape of this buffer is (channels, samples)
    sigbufs, sigparams = read_eeg(INPUTFILE, type=FILETYPE)

    if hlimit == 'end':
        sigbufs = stats.zscore(sigbufs[:sigparams['channels'], llimit:], axis=1)
    else:
        sigbufs = stats.zscore(sigbufs[:sigparams['channels'], llimit:hlimit], axis=1)

    savemat (os.path.join(bundle_dir,'%s_sigbuf.mat' % INPUTFILE), {'zscore': sigbufs, 'params':sigparams})
    log("samples to analyze:\t{}".format(sigbufs.shape[1]))

    # generating the time vector of the recording
    # the time vector is a linear interpolation between the lower time limit and top time limit
    tl = 0
    th = np.shape(sigbufs)[1] / int(sigparams["frequency"])  # the total buffer time is frame_count/frame_rate
    log("recording time (sec):\t{}".format(th))

    # the time vector is a linear space, size equal to buffer time dimension
    # every member is one time sample (th) spaced from the previous
    t = np.linspace(tl, th, np.shape(sigbufs)[1])

    frame_time = t[1] - t[0]
    log("frame time is:\t\t{:10.4f} ms".format(frame_time * 1000))


    results = []
    avares_list = []
    sig_chart = []
    for threshold in TH_RANGE:
        for tau in TAU_RANGE:

            # this is the main line invocating the avalanche analysis on the specified time window (avaTImeWnd):
            # t is the vector for mapping buffer index to literal time
            # tau is the time bin parameter given in number of indicies
            avares = fa(sigbufs, t, tau=tau, threshold=threshold)
            avares_list.append({'tau':tau, 'thresh': threshold, 'avares':avares})
            if avares is not AvalancheNone and avares is not None:
                msigma = avares['sigma'].mean()
                results.append({'tau': tau, 'thresh': threshold,
                                'avsize'        : valOrNone(avares['avalanches'].size),
                                'sigmaM'        : valOrNone(avares['sigma'].mean()),
                                'sigmaSD'       : valOrNone(avares['sigma'].std()),
                                'sigmaVar'      : np.var(avares['sigma']),
                                'sigmaMed'      : np.median(avares['sigma']),
                                'sigmaMin'      : np.amin(avares['sigma']),
                                'sigmaMax'      : np.amax(avares['sigma']),
                                'sigmaRange'    : np.amax(avares['sigma']) - np.amin(avares['sigma']),
                                'scale'         : 1/(np.amax(avares['sigma']) - np.amin(avares['sigma']))
                                })
            else:
                msigma = 0
                results.append({'tau':tau, 'thresh':threshold,
                                'avsize':0,
                                'sigmaM':0,
                                'sigmaSD':0,
                                'sigmaVar':0,
                                'sigmaMed':0,
                                'sigmaMin':0,
                                'sigmaMax':0,
                                'sigmaRange':0,
                                'scale': 1,
                                })


            sys.stdout.write('\r{}:{}'.format(tau,threshold))

    df = pd.DataFrame(results)
    df.to_excel(os.path.join(bundle_dir,'%s_sigmanf.xls' % INPUTFILE), sheet_name='SigmaNF')

    savemat (os.path.join(bundle_dir,'%s_sigmanf.mat' % INPUTFILE), {'ava':avares_list})
    print ("\n%sEND processing%s" % ('*'*10, '*'*10))
