import csv
from numpy import append, nan_to_num
from time import strftime


def nf_stats(sig_mean, sujectNumbr, sessionNumber, itCount, nfparam, stats):
#####
# save outcome to data file
#
    sig_run_mean = 0
    sig_run_sd = 0
    ava_count = 0
    # calculate statistics
    if sig_mean > 0:
        stats = append(stats, sig_mean)
        stats = nan_to_num(stats)
        sig_run_mean = stats.mean()
        sig_run_sd = stats.std()
        ava_count = stats.shape[0]

    # build the record
    record = [
        strftime('%d-%m-%Y %H:%M:%S'),
        itCount,
        sujectNumbr,
        sig_mean,
        sig_run_mean,
        sig_run_sd,
        ava_count,
        float(nfparam),
    ]

    # save the record to a running file
    with open('output/{}_{}.csv.sigma'.format(sujectNumbr, sessionNumber), 'a', newline='', encoding='utf-8-sig') as csvfile:
        outwriter = csv.writer(csvfile, delimiter='\t')
        try:
            outwriter.writerow(record)
        except TypeError:
            print(record)
            csvfile.write(record)

    return stats


def get_scale(stats):
    if (stats.max() - stats.min()) != 0:
        return 1/(stats.max() - stats.min())
    else:
        return 0.09
