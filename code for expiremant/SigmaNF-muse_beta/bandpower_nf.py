import numpy as np
import utils


def bandpower_nf(data_epoch, fs, band_buffer, band):
    # Compute band powers
    band_powers = utils.compute_band_powers(data_epoch, fs)
    band_buffer, _ = utils.update_buffer(band_buffer,
                                         np.asarray([band_powers]))
    # Compute the average band powers for all epochs in buffer
    # This helps to smooth out noise
    smooth_band_powers = np.mean(band_buffer, axis=0)

    # print('Delta: ', band_powers[Band.Delta], ' Theta: ', band_powers[Band.Theta],
    #       ' Alpha: ', band_powers[Band.Alpha], ' Beta: ', band_powers[Band.Beta])

    """ 3.3 COMPUTE NEUROFEEDBACK METRICS """
    # These metrics could also be used to drive brain-computer interfaces

    # Alpha Protocol:
    # Simple redout of alpha power, divided by delta waves in order to rule out noise
    # alpha_metric = smooth_band_powers[band[0]] / smooth_band_powers[band[1]]
    # print('Alpha Relaxation: ', alpha_metric)

    #Beta Protocol:
    nf_metric = smooth_band_powers[3] / smooth_band_powers[1]
    print('Beta Concentration: ', nf_metric)

    # Alpha/Theta Protocol:
    # This is another popular neurofeedback metric for stress reduction
    # Higher theta over alpha is supposedly associated with reduced anxiety
    # theta_metric = smooth_band_powers[Band.Theta] / \
    #     smooth_band_powers[Band.Alpha]
    # print('Theta Relaxation: ', theta_metric)
    return nf_metric, smooth_band_powers
