import numpy as np
import scipy.stats as st
from findavalanche import FindAvalnche as fa, AvalancheNone
from nf_options import TAU


def sigma_nf(wnd, timeVector):
    '''

    :param wnd:
    :param timeVector:
    :return: scalar which is the sigma_nf
    '''
    # z score transform the channels for all channels
    # can use wndz = st.zscore(wnd) but i want to save the channel mean and std
    # wndz = (wnd - wnd_mean) / wnd_std
    wndz = st.zscore(wnd)
    # # run avalanche analysis for the current window.
    avares = fa(wndz.T, timeVector, tau=TAU, threshold=2)

    if avares is None:
        avares = AvalancheNone

    # calculate sigma mean over window
    sig_vec = avares['sigma']
    sig_vec_no_zeros = sig_vec[sig_vec > 0]
    sig_mean = np.mean(sig_vec_no_zeros)

    # deal with nun results
    # TODO: find the reason for nun results in the avalanche analysis and intercept them there
    if str(sig_mean) == 'nan':
        sig_mean = 0.0

    return sig_mean, avares