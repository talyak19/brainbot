# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui\ui_main.ui'
#
# Created by: PyQt5 UI code generator 5.6
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(800, 600)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.cmdRun = QtWidgets.QPushButton(self.centralwidget)
        self.cmdRun.setGeometry(QtCore.QRect(20, 490, 131, 51))
        self.cmdRun.setAutoDefault(False)
        self.cmdRun.setFlat(False)
        self.cmdRun.setObjectName("cmdRun")
        self.txtSubjectNimber = QtWidgets.QPlainTextEdit(self.centralwidget)
        self.txtSubjectNimber.setGeometry(QtCore.QRect(10, 30, 261, 31))
        self.txtSubjectNimber.setObjectName("txtSubjectNimber")
        self.lblSubjectNumber = QtWidgets.QLabel(self.centralwidget)
        self.lblSubjectNumber.setGeometry(QtCore.QRect(10, 10, 47, 13))
        self.lblSubjectNumber.setObjectName("lblSubjectNumber")
        self.toolBox = QtWidgets.QToolBox(self.centralwidget)
        self.toolBox.setGeometry(QtCore.QRect(260, 150, 341, 231))
        self.toolBox.setObjectName("toolBox")
        self.page = QtWidgets.QWidget()
        self.page.setGeometry(QtCore.QRect(0, 0, 341, 177))
        self.page.setObjectName("page")
        self.toolBox.addItem(self.page, "")
        self.page_2 = QtWidgets.QWidget()
        self.page_2.setGeometry(QtCore.QRect(0, 0, 98, 28))
        self.page_2.setObjectName("page_2")
        self.toolBox.addItem(self.page_2, "")
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 800, 21))
        self.menubar.setObjectName("menubar")
        self.menucommand = QtWidgets.QMenu(self.menubar)
        self.menucommand.setObjectName("menucommand")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.actionTest = QtWidgets.QAction(MainWindow)
        self.actionTest.setCheckable(True)
        self.actionTest.setObjectName("actionTest")
        self.actionrunNF = QtWidgets.QAction(MainWindow)
        self.actionrunNF.setObjectName("actionrunNF")
        self.menucommand.addAction(self.actionrunNF)
        self.menucommand.addAction(self.actionTest)
        self.menubar.addAction(self.menucommand.menuAction())

        self.retranslateUi(MainWindow)
        self.toolBox.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.cmdRun.setText(_translate("MainWindow", "run NF"))
        self.lblSubjectNumber.setText(_translate("MainWindow", "subject"))
        self.toolBox.setItemText(self.toolBox.indexOf(self.page), _translate("MainWindow", "Page 1"))
        self.toolBox.setItemText(self.toolBox.indexOf(self.page_2), _translate("MainWindow", "Page 2"))
        self.menucommand.setTitle(_translate("MainWindow", "command"))
        self.actionTest.setText(_translate("MainWindow", "Test"))
        self.actionTest.setToolTip(_translate("MainWindow", "<html><head/><body><p>this is a test action</p></body></html>"))
        self.actionrunNF.setText(_translate("MainWindow", "runNF"))

