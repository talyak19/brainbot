from __future__ import print_function
import numpy as np
import os
import sys
import seaborn as sns
import time

from scipy import stats
from scipy.io import savemat

# self imports
from readEEG import read_eeg
from findavalanche import FindAvalnche as fa, AvalancheNone
from helperPlots import plotSigma

sns.set()
###
# init
# defaults
# windows overlap
WINDOW = 1000  # frames
WNDOVLP = 500  # frames, DELTAt
HISTORY = 2000 # frames, TAUz

# analyis parameters
TAU = 6  # frames

ABS_PATH = r'C:/Users/User/Google Drive/research_data/ROBD/'
FILENAME = r'ROBD_1_clean_EEG.mat'
FILETYPE = 'mat'
SAMPLING_RATE = 250

try:
    if sys.argv[1] == '_DEBUG':
        print("\n\n**********\nDebug Mode\n**********")
        from helperPlots import *

        _DEBUG = True
    else:
        print("\n\n*********\nRun Mode\n**********")
        _DEBUG = False


except:
    _DEBUG = False


def log(msg):
    if _DEBUG:
        print(msg)

if getattr(sys, 'frozen', False):
        # we are running in a bundle
        frozen = 'ever so'
        bundle_dir = sys._MEIPASS
else:
        # we are running in a normal Python environment
        bundle_dir = os.path.dirname(os.path.abspath(__file__))


if __name__ == "__main__":

    llimit = 0
    hlimit = 'end'

    #
    # prepere a buffer from the edf file with initial pre processing
    if ABS_PATH is not None:
        sigbufs, sigparams = read_eeg(os.path.join(ABS_PATH, FILENAME), type=FILETYPE, sampling_rate=SAMPLING_RATE)
    else:
        sigbufs, sigparams = read_eeg(os.path.join(bundle_dir, FILENAME), type=FILETYPE)

    if sigbufs is None:
        print ("file {} cannot be open\n".format(FILENAME))
        sys.exit(1)

    channelsCount = sigparams['channels']
    #sigbufs, sigparams = readedf()
    if hlimit == 'end':
        sigbufs = stats.zscore(sigbufs[:channelsCount, llimit:])
    else:
        sigbufs = stats.zscore(sigbufs[:channelsCount, llimit:hlimit])

    samples_count = sigbufs.shape[1]
    log("samples to analyze:{:50}".format(samples_count))

    # generating the time vector of the recording
    # the time vector is a linear interpolation between the lower time limit and top time limit
    first_sample_time = 0
    buffer_time = np.shape(sigbufs)[1] / int(sigparams["frequency"])  # the total buffer time is frame_count/frame_rate
    log("recording time (sec):{:50}".format(time.gmtime(buffer_time)))

    # the time vector is a linear space, size equal to buffer time dimension
    # every member is one time sample (th) spaced from the previous
    buffer_time_vector = np.linspace(first_sample_time, buffer_time, np.shape(sigbufs)[1]) # total buffer time vector

    # init the wnd buffer with BUFFER rows of channel_count collumns
    wnd = np.zeros((WINDOW, channelsCount))  # x is the x axis of the chart (timescale)

    # initiate time vector in length of WINDOW starting from 0
    wnd_time_vec = np.linspace(0, WINDOW, WINDOW)

    sample_time = buffer_time_vector[1] - buffer_time_vector[0] # time of one sample
    log("frame time is:{:50.4f} ms".format(sample_time * 1000))
    log("window size:{:50.4f} sec".format(WINDOW * sample_time))
    log("window overlap:{:50.4f} sec".format(WNDOVLP * sample_time))
    log("window size:{:50.0f} samples".format(WINDOW))

    # number of windows to analyze given that every window might share data with previous window
    # if WNDOVLP = 0 than windows share no data
    wndCount = np.floor(samples_count / (WINDOW - WNDOVLP))
    log("number of windows:{:50.0f}".format(wndCount))

    wnd_struct = []

    wnd_mean = np.zeros(channelsCount)
    wnd_std = np.zeros(channelsCount)
    a = WNDOVLP / HISTORY

    # call the main avlalanche analysis function
    # return: analysis results dictionary
    for i in np.arange(wndCount):
        sys.stdout.write('.')
        lowLimit = i * WNDOVLP  # multiplication of window number and window indices length
        highLimit = (i + 1) * WNDOVLP

        # read buffer as stream calculate and save into global wnd
        wnd = np.roll(wnd, -1 * WNDOVLP, 0)  # move wnd and clear space for new buffer
        wnd[WNDOVLP:,:] = sigbufs[:, int(lowLimit):int(highLimit)].T  # read lsl samples into moving wnd

        wnd_mean_old = wnd_mean
        wnd_std_old = wnd_std

        wnd_mean = (wnd_mean_old * a) + (np.mean(wnd,0) * (1 - a))
        wnd_std = (wnd_std_old * a) + (np.std(wnd,0) * (1 - a))
        wndz = (wnd - wnd_mean) / wnd_std
        # this is the main line invocating the avalanche analysis on the specified time window (avaTImeWnd):
        # t is the vector for mapping buffer index to literal time
        # tau is the time bin parameter given in number of indicies
        avares = fa(wnd.T, wnd_time_vec, tau=TAU)

        # calculate sigma mean over window
        sig_mean = np.mean(avares['sigma'])

        if avares['indevents'] is None:
            wnd_struct.append({'raw': wnd, 'z': wndz, 'avares': [], 'sigma': 0, 'time': wnd_time_vec})
        else:
            wnd_struct.append({'raw': wnd, 'z': wndz, 'avares': avares, 'sigma': sig_mean, 'time': wnd_time_vec})

    savemat (os.path.join(ABS_PATH, 'data/wnd_struct_001_S1.mat'), {'ava' :wnd_struct})




