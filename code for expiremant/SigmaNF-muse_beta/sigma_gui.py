import sys
import time
from PyQt5.QtWidgets import QApplication, QWidget, QPushButton, QLineEdit, QMainWindow
from PyQt5.QtCore import pyqtSlot
from ui.ui_main import Ui_MainWindow
from Neurofeedback import nf
from threading import Thread

# @pyqtSlot()
class NfUi(QMainWindow, Ui_MainWindow):
    def __init__(self):
        super(NfUi,self).__init__()

        # setup ui from designer
        # the Ui_MainWindow was created by pyuic5 ui/ui_main.ui
        # see http://pyqt.sourceforge.net/Docs/PyQt5/designer.html for details
        self.setupUi(self)

        # member properties

        # Connect up the buttons.
        self.cmdRun.clicked.connect(self.run_nf)

    @pyqtSlot()
    def run_nf(self):
        print (time.ctime());

        nf_runner = nf()
        runner_t = Thread(nf_runner.runNf())
        runner_t.run()




if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = NfUi()
    window.show()
    sys.exit(app.exec_())