clear all;
close all;
channels = 64;

plotwindows = 0;
re = 'struct_(?<SUBJECT>\d{3})_S(?<SESSION>\d{1,4})';
base_dir = 'C:\Users\User\Dropbox\Nir_Geter\sigmaNF\PythonScripts\SigmaNF\output';
output_dir = 'C:\Users\User\Dropbox\Nir_Geter\sigmaNF\analysis_results';

ld = dir(base_dir);
gofdf = cell(length(ld),1);

for f = 1:length(ld)
    if ~ld(f).isdir
        load([base_dir '\' ld(f).name]);
        
        reres = regexp(ld(f).name, re, 'names');
        subject = reres.SUBJECT;
        session = reres.SESSION;
        
        % convert cell array to matrix
        output = cell2mat(lsl_windows);
        sig = zeros(length(output),1);
        for iwnd = (1:length(output))
            tmp = output(iwnd);
            if plotwindows
                figure('Position', [250 280 1000 500])
            
                tmp.events = tmp.avares.indevents;
                tmp.sigvec = tmp.avares.sigma;

                tmp_t = tmp.time;
                scale = max(max(tmp.z)) * 0.5;

                subplot(1,2,1);
                stack_plot(tmp, tmp_t, scale);
                title(['window #' num2str(iwnd)])


                subplot(1,2,2);
                hist(tmp.sigvec);
                xlabel('\sigma') % x-axis label
                title(['window #' num2str(iwnd) ' mean \sigma = ' num2str(tmp.sigma)]);
                saveas(gcf, [base_dir '\figures\wnd_struct_004_S1_' num2str(iwnd) '.jpg']);
                close all;
            end
            sig(iwnd,1) = tmp.sigma;
        end
        
        %remove nan's
        sig(isnan(sig)) = [];
        t = (1:length(sig))';
        [fitresults, gof] = createFit( t, sig, 4 );
        gof.p1 = fitresults.p1;
        gof.session = session;
        gof.subject = subject;
        
        saveas(gcf, [output_dir '\figures\NFReg_SB' subject '_S' session '.pdf']);
        close
        gofdf{f} = gof;

    end
end

%finally remove empty cell array items
gofdf = gofdf(~cellfun('isempty',gofdf));
%turn the cell array into matrix
gofdf = struct2table(cell2mat(gofdf));
writetable(gofdf, [output_dir '\nf_regression.csv']);