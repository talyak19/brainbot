function stack_plot(tmp, tmp_t, scale)
    for c = (1:64)
        z = tmp.z;
        tmp_c = tmp.z(:,c);

        hold on
        plot(tmp_t, tmp_c + (c * scale), '-', 'Color',[0 0 0]+(0.01*c));
        if length(tmp.events) >= c
            tmp_ind = tmp.events(c);
            tind = tmp_ind{1,1};
            
            if ~isempty(tind)
                hold on
                plot(tind + 1, 2 + (c * scale), 'or');
            end
        end
    end
    set(gca,'YTickLabel',[]);
    axis([0 tmp_t(end) 0 66 * scale ]);
    xlabel('time msec');
end
