# functions dealing with commandline options
import getopt
import sys
from numpy import float64
###
# init
# defaults
# windows overlap
WINDOW = 1024  # frames
WNDOVLP = 512  # frames, DELTAt
HISTORY = 2048  # frames, TAUz

# buffer to read on each inlet pull
BUFF = WINDOW - WNDOVLP  # frames
MAX_BUFLEN = 360  # samples: lsl definition
MAX_CHUNKLEN = 0  # samples: lsl definition for chunk communication. set to 0 for sample by sample

# analyis parameters
TAU = 6  # frames
CHANNEL_IND = [1]
PROTOCOL = 1  # beta/theta=1, sigma=2
PROTOCOL_DIRECTION = 1  # upregulating=1, downregulating = 2
SET_DYN_THRESHOLD = 30  # set dynamic threshold every 30 samples

# socket host and port
HOST = "127.0.0.1"
PORT = 4510

RUNTIME = 1200  # seconds
NFTHRESH = 0
LOG_TO_CONSOLE = False
# scale is the factor of sigma multiplication for proper NF representation.
# this will also set the regulation direction.
# together with scale this parameter sets the behavior of the NF presentation
# if direction is negative - the goal will be to lower the sigma
# take minimum sigma divided by maximum sigma
SCALE = 0.0539
NF_MIN = 0.50

NOTCP = False
SUBJECT = '0000'
SESSION = 0


def opt_threshold(a):
    global NFTHRESH
    NFTHRESH = a


def opt_no_ip():
    global NOTCP
    NOTCP = True
    print("\nWorking in no Unity mode\n")


def opt_host(a):
    global HOST
    HOST = a


def opt_subject(a):
    global SUBJECT
    SUBJECT = a
    print("*\tSubject number set to:{}".format(SUBJECT))


def opt_session(a):
    global SESSION
    SESSION = a
    print("*\tSession number set to:{}".format(SESSION))


def opt_runtime(a):
    global RUNTIME
    RUNTIME = float64(a)
    print("*\tWill run for {} seconds".format(RUNTIME))


def opt_tau(a):
    global TAU
    TAU = float64(a)
    print("*\tTau set to {}".format(TAU))


def opt_help():
    usage()


def opt_scale(a):
    global SCALE
    SCALE = float64(a)
    print("*\tscALE set to {}".format(SCALE))


def opt_nfmin(a):
    global NF_MIN
    NF_MIN = float64(a)
    print("*\tNF_MIN set to {}".format(NF_MIN))


def opt_proto(a):
    global PROTOCOL
    if a == 'sigma':
        PROTOCOL = float64(2)
    else:
        PROTOCOL = float64(1)
    print("*\tProtocol set to {}".format(a))


ops_list = ['threshold=', 'no-tcp', 'subject=', 'runtime=', 'tau=', 'host=', 'help', 'scale=', 'session=', 'nf-min=', 'protocol=']
ops_dict = {
    '--threshold': opt_threshold,
    '--no-tcp': opt_no_ip,
    '--subject': opt_subject,
    '--runtime': opt_runtime,
    '--tau': opt_tau,
    '--host': opt_host,
    '--scale': opt_scale,
    '--help': opt_help,
    '--session': opt_session,
    '--nf-min': opt_nfmin,
    '--protocol': opt_proto
}


def usage():
    print("""
    --threshold=    threshold of findevents. default .03
    --no-tcp        for debug - do not send result over tcp to Unity
    --subject=      subject number. default 0000
    --runtime=      time of running. default 10 seconds
    --tau=          the frames delta for binning. default 5
    --host=         the ip address of the client machine default=127.0.0.1
    --scale=        the multiplication factor for NF representation
    --session=      the current session of training default=0
    --nf-min=       minimum nfparam 
    --protocol=     sigma | beta
    --help          show this text
    """)


def processCommandlineOptions():
    # get init options from command line
    try:
        opts, args = getopt.getopt(sys.argv[1:], 'x', ops_list)
    except getopt.GetoptError as err:
        # print help information and exit:
        print(str(err))  # will print something like "option -a not recognized"
        usage()
        sys.exit(2)

    # activate opts functions
    for opt, a in opts:
        if a == '':
            ops_dict[opt]()
        else:
            ops_dict[opt](a)
