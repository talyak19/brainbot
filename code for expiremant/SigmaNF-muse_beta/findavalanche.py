import scipy as sp
import numpy as np
import sys

from numpy import diff, where, r_, size

AvalancheNone = {'indevents':[], 'timeevents':[], 'channelHist': np.ndarray(0), 'dt': 0, 't_grid': np.ndarray(0), 'tp': 0, 'tp_bin': 0,
        'sigma': np.ndarray(0), 'avalanches': np.ndarray(0)}


def FindAvalnche(buff, time, tau=None, threshold=3, method='max'):
    # TODO: RASTER calculation - after implementing remove to dedicated function
    t_end = time[-1]
    if tau is None:
        dt = (time[1] - time[0])
    else:
        dt = (time[1] - time[0]) * tau  # tau is expressed in multiplication of one frame time

    # Calculate grid with bins of size tau
    # this will result in an array with time stamps
    # for each bin in the time grid
    t_grid = np.arange(0, t_end, dt)
    #gridBins = t_grid.size TODO: DEPRACTICATED


    # initiating memory spaces for the timeevents representation
    # and the histogram representation
    timeevents = []
    indevents = []
    channel_time_hist = np.zeros((buff.shape[0], t_grid.size - 1))


    # iterating over the channels:
    # for each channel with index i and channel data
    for i, channel in zip(range(buff.shape[0]), buff):

        # this will return the indices of events in the channel
        indevents.append(findevents(channel=channel, threshold=threshold, method=method))

        # if indevents is None than this time window is free of events
        # the loop should therefore be continued without further processing
        if indevents[-1] is None:
            continue

        # here we will convert the event indices vector into time vector
        # e.g. replace indicies of events with their corresponding time
        # 'timeevents' is on the unmodified/unbinned timescale
        timeevents.append(time[indevents[-1]])

        # calculating avalanches by aggregating events in time bins
        # time bins defined by gridBins vector
        channel_time_hist[i, :] = (np.histogram(timeevents[-1], t_grid))[0]

    # TODO: remove noizy channels

    # TODO: [optional] loop over timescales of analysis

    #### Avalanches segmentation and sigma calculations

    # collapse to a vector with number of events from all channels in each bin
    # here the space of channels translated to one vector with the channels information compressed
    # this vector will be re use when calculating sigma
    tp = channel_time_hist.sum(0)

    # create a binary vector of empty vs. non-empty bins
    # this vector will be at the size of time grid with one dimention
    # here the size of the bin is ignored and compressed to a binary representation
    # 1 is time bin representing one or more events in one or more channels
    tp_bin = (tp > 0) * 1  # TODO: think about an avalanche threshold - e.g. take bins with 0ne event as non avalanche

    # create vector of edges of avalanches(1 when avalanche starts, -1 when avalanche ends)
    # this is a segmentation of the vector into strings of avalanches avalanche statrts with 1 and ends with 1-
    tp_edges = diff(tp_bin)

    # find starting point of the first avalanche
    ind_first1 = where(tp_edges == 1)
    if ind_first1[0].size >= 1:
        ind_first1 = ind_first1[0][0]
    else:
        return AvalancheNone  # if no avalanche detected return False


    # set everything before the first avalanche to 0 (in case we had the end of a previous avalanche)
    tp_edges[0:ind_first1] = 0

    # bins where avalanches start
    ind_av_start = r_[where(tp_edges == 1)]

    # bins where avalanches end
    ind_av_end = r_[where(tp_edges == -1)]

    N_avalanches = r_[ind_av_start.size, ind_av_end.size].min()  # number of avalanches
    ind_av_start = ind_av_start[0:N_avalanches]  # indices where avalanches start
    #  ind_av_end = ind_av_end[0:N_avalanches]  # indices where avalanches end

    sigma_vec = np.zeros(N_avalanches)
    for n_av in range(N_avalanches):  # loop over avalanches
        av_bin1 = ind_av_start[n_av] + 1  # first bin

        # TODO: uncomment code from matlab for more elaborated analysis
        # av_bin2 = ind_av_end(n_av) # last bin
        # av_label[av_bin1:av_bin2] = n_av # label all the bins of this avalanche with the serial number of this avalanche
        # av_size_vec(n_av) = sum(tp(av_bin1:av_bin2)) # sum events from all bins in this avalanche to calculate the size
        # av_dur_vec(n_av) = sum(tp_bin(av_bin1:av_bin2)) # find duration of avalanche

        # estimate sigma by using tp (which is the histogram collapsed channel events)
        # e.g. a count of event for each time bin across channels
        N_peaks1 = tp[av_bin1]  # number of events in first bin ("ancestors")
        N_peaks2 = tp[av_bin1+1]  # number of events in second bin ("decendants")
        sigma_vec[n_av] = N_peaks2/N_peaks1 # ratio of decendants to ancestors

    return {
        'indevents': indevents,
        'timeevents': timeevents,
        'channelHist': channel_time_hist,
        'dt': dt,
        't_grid': t_grid,
        'tp': tp,
        'tp_bin': tp_bin,
        'sigma': sigma_vec,
        'avalanches': ind_av_start,
    }

# channel must be already converted to z transform
def findevents(channel, threshold=3, method='max'):

    ex = (channel >= threshold) * 1  # find indexes of excursions above threshold
    edges = diff(ex)  # find edges of excursions

    #  cleaning tail partial events
    firstup = where(edges == 1)  # find the first excursion
    if np.size(firstup) == 0:
        return np.ndarray(0, dtype=int)  # if no events found, return empty array

    firstup = firstup[0][0]
    edges[0:firstup] = 0  # remove edges before first up excursion

    # determining borders of excursions (indexes)
    edge_start = r_[where(edges == 1)]
    edge_end = r_[where(edges == -1)]
    tmpn = sp.minimum(size(edge_start), size(edge_end))

    if tmpn == 0:
        #  when tmpn is empty there is no evenets in this time window
        return []  # return None to signal empty window

    edge_start = edge_start[0:tmpn]
    edge_end = edge_end[0:tmpn]

    ind_max = sp.zeros([1, tmpn], dtype=int)[0]

    for n in np.arange(0, tmpn, dtype=int):
        y_tmp = channel[edge_start[n]:edge_end[n]]  # extract single excursion
        if method == 'max':
            ind_max_tmp = where(y_tmp == max(y_tmp))[0][0]  # find the index of maximal point
        elif method == 'median':
            raise Exception('Median method not implemented yet')
            # TODO: implement median method
        elif method == 'midrange':
            raise Exception ('midrange method not implemented yet')
            # TODO: implement midrange method

        ind_max[n] = edge_start[n] + ind_max_tmp - 1  # compute index of maximal point in the full signal
    return ind_max

